# Frick BSP

## Usage

### 1. Clone the repository

```bash
git clone https://bitbucket.org/frick/frick-bsp
```

### 2. cd into frick-bsp

```bash
cd frick-bsp
```

### 3. Run initialize

```bash
source initialize
```
You will be asked for your name and your email during the process, just press enter to accept the default values. When asked to verify just press ```y``` and then enter. At the end of the process it will ask you to accept the EULA, keep pressing space until you get the end of the document and then press ```y``` and then enter.

Throughout this process all the necessary files will be pulled down and all of the configurations will be added.

### 4. Build the image

```bash
bitbake frick-image
```

### 5. Unzip the sdcard img

```bash
gunzip -c tmp/deploy/images/jciq6/frick-image-jciq6.sdcard.gz > /tmp/frick-image-jciq6.sdcard
```

### 6. Unmount SD card
After you insert your SD card unmount it by running the following command

#### Mac

1. Determine which device is mounted by running ```mount``` and looking for the
   name of the drive. The device will have the prefix ```/dev/disk```. We will
   use the device ```/dev/disk2s1``` as our example.
2. Run the command ```sudo diskutil umountDisk DEVICENAME``` where DEVICENAME is
   the name of your device up to the first number in the name. In our example we would run 
   ```sudo diskutil umountDisk /dev/disk2```.

#### Linux

1. Determine which device is mounted by running ```mount``` and looking for the
   name of the drive. The device will have the prefix ```/dev/```. We will
   use the device ```/dev/sdc1``` as our example.
2. Run ```sudo umount DEVICENAME*``` where DEVICENAME is the device with number
   trimmed from the end of the name. In our example we would run ```sudo umount
   /dev/sdc*```


### 7. Burn the sdcard img to the card

#### Mac

```bash
sudo dd if=/tmp/frick-image-jciq6.sdcard of=/PATH/TO/rDEVICE bs=1m
```

If our device is ```/dev/disk2``` then we would run ```sudo dd if=/tmp/frick-image-jciq6.sdcard of=/dev/rdisk2 bs=1m```

#### Linux

```bash
sudo dd if=/tmp/frick-image-jciq6.sdcard of=/PATH/TO/DEVICE bs=1M
```

If our device is ```/dev/sdc``` then we would run ```sudo dd if=/tmp/frick-image-jciq6.sdcard of=/dev/sdc bs=1M```


---

## Change what is getting pulled
You need to modify frick-image.xml or itg-manifest.xml to change what is being pulled.
frick-image.xml should be modified if you need to pull something new with respect to ```meta-frick```.
itg-manifest.xml should be modified if you need to pull something new with respect to ITG's Yocto build.

The documentation for changing the manifest file can be found at [https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.txt](https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.txt)

---

## What initialize is doing
When you call ```source initialize``` the following occurs:

1. Install the repo tool
2. Get the freescale bsp by running ```repo init https://github.com/Freescale/fsl-community-bsp-platform -b krogoth```
3. Copy the frick-image.xml and itg-manfiest.xml to .repo/local_manifests
4. Pull down all the necessary files to do a Yocto build using ```repo sync```
5. Include jciq6 files in the conf file by running ```bash jciq6-setup.sh```
6. Add frick specific configurations to the conf file by running ```bash frick-setup.sh```
7. Setup the build environment by running ```MACHINE=jciq6 source setup-environment build```

---

## How to reset after running initialize.sh
Run ```git -x -d -f``` from the projects root directory

---

## Building on AWS
To build the frick-image on AWS do the following for these steps. We will be assume that you will be working from ```$HOME``` on both the server and the local machine.

### 1. Start SSH Server

1. Navigate to [https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances) and Login. Mike has the credentials for the AWS account.
2. Right click on the row the has the name ```Yocto```
3. Select ```Instance State``` then ```Start``` on the menu. 
![Yocto Start](docs/yocto-start.png)

### 2. Get SSH key
Download ```Yocto.Pem``` from the frickcontrols dropbox account. It can be found at [https://www.dropbox.com/home/Frick%20Controls%20Shared/AWS%20Credentials](https://www.dropbox.com/home/Frick%20Controls%20Shared/AWS%20Credentials). Mike has the credentials for the dropbox account.

After the file is downloaded you will need to change the permissions of ```Yocto.pem``` to ```600``` by running ```chmod 600 /PATH/TO/Yocto.pem```

### 3. SSH into AWS server
Run ```ssh -i /PATH/TO/Yocto.pem ubuntu@34.196.127.13``` to ssh into the AWS server.
Note: When you ssh into the server run the command ```sudo shutdown -h 18:00`` to have the server auto shutdown at 6:00 pm.

### 4. Get frick-bsp and build the image
Follow steps 1-4 under **Usage**.

### 5. Copy the image to your local machine
Run ```scp -C -i /PATH/TO/Yocto.pem ubuntu@34.196.127.13:frick-bsp/build/tmp/deploy/images/jciq6/frick-image-jciq6.sdcard.gz .``` to copy the file to your local machine.


### 6. Unzip the file
On your local machine run ```gunzip -c frick-image-jciq6.sdcard.gz > /tmp/frick-image-jciq6.sdcard``` to unzip the image.

### 7. Unmount and Burn
Follow steps 6-7 under **Usage** to burn the image to the SD card.



